# Getting Started 
Go to this URL and clone project (or download).
[https://bitbucket.org/mykedargyle/projects/src/master/](https://bitbucket.org/mykedargyle/projects/src/master/)


## Clone Projects
Open Terminal and navigate to any folder. Type:
`git clone https://MDArgyle@bitbucket.org/mykedargyle/projects.git`


## Run Project
After project is cloned locally, while still inside terminal, navigate to the `dashboard` folder within the project you just downloaded.
Once inside, `npm install` to get the dependencies.


## Available Scripts
Inside the project directory, you can run: `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

You should at this point see the dashboard page running locally.
