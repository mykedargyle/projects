import React from 'react'

function Breadcrumb() {
  return (
    <div className="breadcrumb-wrapper">
      <div className="tier-one">
        <i className="icon fas fa-cubes"/> <a href="/">UX</a>
      </div>

      <div className="page-name">
        <i className="icon fas fa-cubes"/> Diagram Sample
      </div>
    </div>
  );
}

export default Breadcrumb;
