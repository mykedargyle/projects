import React from 'react'
import Button from '../patterns/Button'
import ZoomTool from '../patterns/ZoomTool'

function StatusBar() {
  return (
    <div className="status-bar-wrapper">
      <p>Working Draft <i className="icon fas fa-info-circle"/></p>

      <div>
        <Button label="Check In" className="white-button"/>
        <Button label="Undo Check Out" className="text-button"/>
      </div>

      <ZoomTool/>
    </div>
  );
}

export default StatusBar;
