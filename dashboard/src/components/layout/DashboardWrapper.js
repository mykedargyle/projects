import React from 'react'
import Breadcrumb from './Breadcrumb'
import StatusBar from './StatusBar'
import Dashboard from '../dashboard/Dashboard'

function DashboardWrapper() {
  return (
    <main>

      <Breadcrumb/>
      <StatusBar/>
      <Dashboard/>

    </main>
  );
}

export default DashboardWrapper;
