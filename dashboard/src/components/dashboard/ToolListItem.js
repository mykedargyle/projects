import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames'


class ToolListItem extends Component {
  render() {
    const {
      className, label
    } = this.props;

    const wrapperClass = classnames('icon', className)

    return (
      <li className="tool-list-item">
        <i className={wrapperClass}/>
        <p>{label}</p>
      </li>
    )
  }
}

ToolListItem.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string
}

export default ToolListItem
