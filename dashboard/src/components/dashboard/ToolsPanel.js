import React from 'react'
import ToolListItem from './ToolListItem'

function ToolsPanel() {
  return (
    <div className="tools-panel">
      <ul>
        <ToolListItem className="far fa-shapes" label="Shape" />
        <ToolListItem className="far fa-lasso" label="Lasso" />
        <ToolListItem className="fal fa-object-group" label="Select All" />
        <ToolListItem className="fas fa-images" label="Insert" />
        <ToolListItem className="fas fa-table" label="QuickMap" />
        <ToolListItem className="fas fa-horizontal-rule" label="Line" />
        <ToolListItem className="fal fa-text-size" label="Text" />
        <ToolListItem className="fas fa-comment" label="Comment" />
        <ToolListItem className="fas fa-search" label="Find" />
      </ul>

      <ul>
        <ToolListItem className="fas fa-sticky-note" label="Narative" />
        <ToolListItem className="far fa-cog" label="Setting" />
        <ToolListItem className="fas fa-ellipsis-h" label="More" />
        <ToolListItem className="fas fa-globe" label="Multilingual" />
      </ul>
    </div>
  );
}

export default ToolsPanel;
