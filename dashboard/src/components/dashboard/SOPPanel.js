import React from 'react'
import SOPPanelListItem from './SOPPanelListItem'

function SOPPanel() {
  return (
    <div className="sop-panel">
      <ul>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
        <SOPPanelListItem/>
      </ul>

      <div className="sop-actions">
        <a>View SOP</a>
        <a>expand All</a>
      </div>
    </div>
  );
}

export default SOPPanel;
