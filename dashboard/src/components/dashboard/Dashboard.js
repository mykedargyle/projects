import React from 'react'
import ToolsPanel from './ToolsPanel'
import SOPPanel from './SOPPanel'

function Dashboard() {
  return (
    <div className="dashboard-interior">

      <ToolsPanel/>

      <div className="canvas">

      </div>

      <SOPPanel/>

    </div>
  );
}

export default Dashboard;
