import React from 'react'

function SOPPanelListItem() {
  return (
    <li>
      <i className="icon fas fa-location-arrow" />
      <span>Navigation</span>
      <i className="icon fas fa-caret-down accordion-dropdown" />
    </li>
  );
}

export default SOPPanelListItem;
