import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames'


class Button extends Component {
  render() {
    const {
      className, label
    } = this.props;

    const wrapperClass = classnames('button', className)

    return (
      <button className={wrapperClass}>{label}</button>
    )
  }
}

Button.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string
}

export default Button
