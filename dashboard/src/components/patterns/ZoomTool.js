import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames'


class ZoomTool extends Component {
  render() {
    const {
      className, label
    } = this.props;

    const wrapperClass = classnames('zoom-tool', className)

    return (
      <div className={wrapperClass}>
        <button className="zoom-out">-</button>
        <div className="zoom-value">45%</div>
        <button className="zoom-in">+</button>
      </div>
    )
  }
}

ZoomTool.propTypes = {
  // className: PropTypes.string,
  // label: PropTypes.string
}

export default ZoomTool
