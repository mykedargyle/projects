import React from 'react'
import DashboardWrapper from './components/layout/DashboardWrapper'

function App() {
  return (
    <div className="dashboard">

      <header/>

      <div className="main-content">
        <aside/>

        <DashboardWrapper/>
      </div>
    </div>
  );
}

export default App;
